FROM docker.io/debian:11

RUN     apt-get update && \
        apt-get install -y \
            ansible \
            python3-boto3 \
            && \
        ansible-galaxy collection install amazon.aws && \
        rm -Rf /root/.ansible/tmp/* && \
        apt-get clean && apt-get autoclean

COPY    ansible/ansible.cfg ~/.ansible.cfg