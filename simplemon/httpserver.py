#!/usr/bin/env python3
from http.server import BaseHTTPRequestHandler, HTTPServer
import psutil
import json


class HTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        values = {
            "cpu_times":    psutil.cpu_times()._asdict(),
            # "load_avg":     dict(zip((1,5,15),psutil.getloadavg())),
            "virtual_mem":  psutil.virtual_memory()._asdict(),
            "swap_mem":     psutil.swap_memory()._asdict(),
            "disk_usage":   
                dict(((x.mountpoint, psutil
                    .disk_usage(x.mountpoint)
                    ._asdict()) for x in psutil.disk_partitions())),
        }    
        self.wfile.write(json.dumps(values).encode('utf8'))
        self.end_headers()

def main():
    server = HTTPServer(("127.0.0.1", 9000), HTTPRequestHandler)
    print("Initializing server")
    server.serve_forever()

if __name__ == '__main__':
    main()