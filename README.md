# Example of AWS EC2 instance with Nginx (IaC)
> [Versão em português](./README.pt-br.md)

> It has been assumed that you have Ansible and Terraform already installed in your system.
>
> An there's no need to say that I'm not responsible for any charge that can occur using this project. Right?

This project aims to create AWS EC2 Instance using Terraform tool, as well define its Security Group and enable a ssh key to manage it with Ansible.

With Ansible we'll install and configure Nginx services and a simple system monitor.

## Requirements
We will need some complements for Terraform and Ansble that can be found in their own repositories.

### Requirements for Ansible
To allow us to edit the AWS Security Group we must install the _amazon.aws_ collection. To do that, execute the following command:
```bash
ansible-galaxy collection install amazon.aws
```
This complement needs the _boto3_ python library. So for debian-like systems just execute 
<code>apt install python3-boto3</code>

We also need to enable the __aws_ec2__ Ansible inventory plug-in, for this edit the configuration file, mostly "~/.ansible.cfg", and insert "aws_ec2" in "_enable_plugin_" inside "_inventory_" section.

```ini
[inventory]
enable_plugins = aws_ec2
```

### Terraform requirements
To be integrated with AWS, Terraform uses the _hashicorp/aws_ provider.
So just get inside "terraform" folder of this project and execute <code>terraform init</code> and that complement will be installed.

### Credentials and keys

#### AWS Access Key
We need an AWS user account with privilegies to create and edit EC2 instances and Security Groups. 
And then we must have its AWS access key id and security key. 

Our tools can access these informations throug environment variables, as shown bellow.
```bash
$ export AWS_ACCESS_KEY_ID=AK************IEVXQ
$ export AWS_SECRET_ACCESS_KEY=gbaIbK*********************iwN0dGfS
```
Or through AWS configuration file (___~/.aws/credentials___), it should have  content similar to:
```ini
[default]
aws_access_key_id = AK************IEVXQ
aws_secret_access_key = gbaIbK*********************iwN0dGfS
```

#### SSH Key
Ansible works through SSH protocol, so to generate ssh key pairs you can use the command bellow:
```bash
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa_aws-devops
```

# Instructions
With the requirements met, it's possible to customize some properties before run the tools.

To do so, edit the "terraform/vars.tf" file of this project changing any value you want to.
```ini
variable "props" {
  type = map
  default = {
    region = "us-east-1" # AWS Region
    security_group_name = "http-only" # AWS Security Group name to create/use
    public_ip = true # Whether to give a public IP or not.
    ami = "ami-04505e74c0741db8d" # AMI image to use, Ubuntu 20.04 LTS x86 in this case
    instance_type = "t2.micro" # EC2 instance type
    nodes_num = 1 # How many instances to create
    tag_name = "devopschallange" # Give a Tag name to identify the AWS Resources
    ssh_pubkey = "~/.ssh/id_rsa_aws-devops.pub" # Which ssh public key to use.
  }
}
```
> __IMPORTANT__
>
> Everytime that you chance values for __region__, __security_group_name__ and __tag_name__ you must check the files:
> * __ansible/variables.yml__
> * __ansible/aws_ec2.yaml__

## Time to Run
### Terraform
Get inside "_terraform_" folder and execute:
```bash
$ terraform plan
$ terraform apply # answer "yes" if you agree with the changes
```
The steps took by the Terraform are:
1. Register the choosen ssh public key on AWS;
1. Create the EC2 Security Group with only HTTP (TCP 80) allowed;
1. Then it'll create the EC2 instance.

At the end the public IP address will be shown.

### Ansible
For Ansible, just get inside "_ansible_" folder and execute the command informing the ssh private key:
```bash
$ ansible-playbook -i aws_ec2.yaml --private-key ~/.ssh/id_rsa_aws-devops playbook.yml
```
Ansible steps are:
1. Add permission to access ssh (TCP 22) to the Security Group;
1. Copy files for a simple service monitor;
1. Register the service monitor as a SystemD service;
1. Starts this service;
1. At the end it removes the ssh access permission.

# Gitlab Pipeline
The file "_.gitlab-ci.yml_" specify how CI/CD will run.

This pipeline will run Ansible insade a container. Its steps/jobs includes:
1. The pipeline will be triggered only when there's a change inside "_simplemon_" folder;
1. A SAST scan will run on all projetct content;
1. The Ansible playbook will be executed.

The private key must be added as a file variable named as "__privkey_file__" in CI/CD Properties of Gitlab.