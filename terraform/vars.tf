variable "props" {
  type = map
  default = {
    region = "us-east-1"
    security_group_name = "http-only"
    public_ip = true
    ami = "ami-04505e74c0741db8d" # Ubuntu 20.04 LTS x86
    instance_type = "t2.micro"
    nodes_num = 1
    tag_name = "devopschallenge"
    ssh_pubkey = "~/.ssh/id_rsa_aws-devops.pub"
  }
}