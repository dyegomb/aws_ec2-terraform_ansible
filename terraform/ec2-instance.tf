terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.7"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = lookup(var.props, "region")
}

### Some gathering
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

### SSH Key
resource "aws_key_pair" "devops" {
  key_name   = "ssh-key"
  public_key = file(lookup(var.props, "ssh_pubkey"))
  # public_key = file("~/.ssh/id_rsa_aws-devops.pub")
}

### Networking

resource "aws_security_group" "sgdevops" {
  name = lookup(var.props, "security_group_name")
  description = lookup(var.props, "tag_name")
  vpc_id = aws_default_vpc.default.id

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = lookup(var.props, "tag_name") 
  }
}

#### EC2 instance
resource "aws_instance" "webserver" {
  ami           = lookup(var.props, "ami")
  instance_type = lookup(var.props, "instance_type")
  key_name      = aws_key_pair.devops.key_name
  associate_public_ip_address = lookup(var.props, "public_ip")
  security_groups = [ aws_security_group.sgdevops.name ]
  count = lookup(var.props, "nodes_num")

  tags = {
    Name = lookup(var.props, "tag_name") 
  }
}

output "ec2instance" {
  value = aws_instance.webserver[*].public_ip
}
