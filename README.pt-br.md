# Devops Challenge
>This is a challenge by Coodesh

Esse projeto tem a finalidade de criar uma instância AWS EC2 com a ferramenta Terraform, bem como definição de seu Security Group e habilitação de chave ssh para acesso ssh.

Com o Ansible são instalados e configurados os serviços Nginx e um monitor simples de recursos do sistema.

## Pré-resquisitos
Esse projeto necessita das ferramentas Terraform e Ansible, onde foram utilizados complementos disponíveis em seus próprios repositórios.

### Requisitos para Ansible
Para ser possível editar o AWS Security Group é necessário instalar o componente a coleção _amazon.aws_, para isso utilize o comando:
```bash
ansible-galaxy collection install amazon.aws
```
Esse complemento necessita que a biblioteca boto3 do python esteja instalada. Para sistemas operacionais Debian-like pode ser instalado com o comando <code>apt install python3-boto3</code>

É também necessário habilitar o plugin __aws_ec2__ 
nas opções de inventário do Ansible, para tanto certifique-se que o arquivo _ansible.cfg_ possua algo próximo ao exemplo abaixo:
```ini
[inventory]
enable_plugins = aws_ec2
```

### Terraform
No Terraform é utilizado o "provider" _hashicorp/aws_ para interações com a AWS.

Para instalar esse recurso basta acessar a pasta "terraform" deste projeto e executar <code>terraform init</code>

### Credenciais e chaves
É necessário acessar a "access key" de uma conta de usuário AWS com permissões para criação de instâncas EC2 e Secutiry Groups.

####
A "access key" pode ser informada através das variáveis de ambiente:
```bash
$ export AWS_ACCESS_KEY_ID=AK************IEVXQ
$ export AWS_SECRET_ACCESS_KEY=gbaIbK*********************iwN0dGfS
```
Ou através de arquivo de configuração aws, como exemplo no arquivo "__~/.aws/credentials__" com o conteúdo abaixo.
```ini
[default]
aws_access_key_id = AK************IEVXQ
aws_secret_access_key = gbaIbK*********************iwN0dGfS
```

#### Chave ssh
Para utilizar a ferramente Ansible é necessário gerar um par de chaves assimétricas ssh, isso pode ser realizado com o comando abaixo.
```bash
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa_aws-devops
```

# Instruções de uso
Após atendidos os pré-requisitos, é possível personalizar alguns ajustes antes de executar as ferramentas de fato.

No arquivo "terraform/vars.tf" é possível fazer os seguintes ajustes:
```ini
variable "props" {
  type = map
  default = {
    region = "us-east-1" # AWS Region
    security_group_name = "http-only" # Nome do EC2 Security Group a ser utilizado
    public_ip = true # Se será definido um IP público à instância
    ami = "ami-04505e74c0741db8d" # AMI com Ubuntu 20.04 LTS x86
    instance_type = "t2.micro" # Tipo da instância EC2
    nodes_num = 1 # Número de instâncias EC2 a serem criadas
    tag_name = "devopschallange" # Nome de TAG para facilitar identificação dos recursos
    ssh_pubkey = "~/.ssh/id_rsa_aws-devops.pub" # Local da chave ssh pública a ser utilizada pelo Ansible
  }
}
```
> __IMPORTANTE__
>
> Sempre que o arquivo __terraform/vars.tf__ for alterado, verifique se as suas informações estão condizentes com as informações nos arquivos:
* __ansible/variables.yml__
* __ansible/aws_ec2.yaml__

Ao fim do deploy o acesso à pagina estática estará disponível através de __http://{IP-EC2}/__

Os dados de estado da instância estarão acessível em __http://{IP-EC2}/status__

## Execução das ferramentas
### Terraform
Acesse o diretório "_terraform_" e execute:
```bash
$ terraform plan # Confira os passos a serem executados
$ terraform apply # Escreva "yes" se concordar com as alterações
```
Suas etapas são:
1. Cadastro de chave ssh pública em AWS;
1. Criação de Security Group com permissão apenas para porta HTTP (TCP 80);
1. Cria nova(s) instância(s) EC2. 

Ao final será informado o endereço IP das instâncias criadas.

### Ansible
Acesse o diretório "_ansible_" e execute:
```bash
$ ansible-playbook -i aws_ec2.yaml --private-key ~/.ssh/id_rsa_aws-devops playbook.yml
```
As etapas no playbook são:
1. Adição de permissão a acesso ssh (TCP 22) ao Security Group da instância EC2;
1. Copia dos arquivos do serviço de monitoramento (simplemon);
1. Cadastro do "simple" como serviço no SystemD;
1. Inicialização de seu serviço;
1. Remoção da permissão de acesso SSH do Security Group.

# CI/CD Pipeline
Utilizei a solução CI/CD do Gitlab, ela é realizada através do arquivo "_.gitlab-ci.yml_".

Optei por utilizar o próprio Ansible para executar o processo de deploy, portanto uma imagem docker pode ser criada com as instruções em "Dockerfile" que fornece acesso a ferremanta "ansible-playbook".

Essa pipeline envolve as seguintes etapas:
1. O pipeline somente será executado quando houver alterações no diretório "_simplemon_";
1. Teste SAST em todo o projeto com o template fornecido pela Gitlab;
1. Execução do playbook Ansible.
A chave ssh privada deve ser informada como uma variável do tipo "file" com o nome "__privkey_file__" nas propriedades CI/CD do Gitlab;